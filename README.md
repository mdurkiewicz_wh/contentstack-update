# contentstack-update

Update CS content.

## how to use

```
yarn
CMS_CONFIG=[MC|GAMING] yarn update-integration
```

## development
In service directory you should have all required tools to do necessary interactions. If not available then it is a good place to add it.

### fetchAllContentstackGames
downloads all games in batches. In result you have an array with all games records you can then filter out.

```
const { cs } = require("src/services/contentstack");

const games = await cs.fetchAllContentstackGames();
```

### updateGameEntry
updates single game entry with required parameters.

```
const { cs } = require("src/services/contentstack");

await cs.updateGameEntry(game.uid, {
  title: "New title",
  // any other paramenters
});
```

### publishContentstackGames
publish all entries with specific version.

```
const { cs } = require("src/services/contentstack");

const entries = [ game1, game2, game3, ...];

await publishContentstackGames(entries, [env.uid]);
```
