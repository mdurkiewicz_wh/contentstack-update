const axios = require("axios");
const chalk = require("chalk");
const { chunk } = require("lodash");
// const { saveJSONFile, getFormattedTime } = require('./utils');
const log = console.log;

async function updateGameEntry(apiKey, managementToken, entryUID, entryData) {
  log({
    url: `https://eu-api.contentstack.com/v3/content_types/game/entries/${entryUID}`,
    entryData,
  });
  const response = await axios({
    method: "PUT",
    headers: {
      api_key: apiKey,
      authorization: managementToken,
      "Content-Type": "application/json",
    },
    // url: `https://eu-api.contentstack.com/v3/content_types/launch_configuration/entries/${entryUID}`,
    url: `https://eu-api.contentstack.com/v3/content_types/game/entries/${entryUID}`,
    data: {
      entry: entryData,
    },
  });

  return response.data;
}

async function fetchContentstackGames(apiKey, managementToken, params = {}) {
  const response = await axios(
    // 'https://eu-api.contentstack.com/v3/content_types/launch_configuration/entries',
    "https://eu-api.contentstack.com/v3/content_types/game/entries",
    {
      params,
      headers: {
        api_key: apiKey,
        authorization: managementToken,
        "Content-Type": "application/json",
      },
    }
  );
  return response.data;
}

async function fetchAllContentstackGames(
  apiKey,
  managementToken,
  queryParams = {}
) {
  const initialData = await fetchContentstackGames(
    apiKey,
    managementToken,
    queryParams
  );
  const { count, entries: initialGames } = initialData;
  const resultGames = [...initialGames];

  for (let i = 100; i < count; i = i + 100) {
    const { entries: games } = await fetchContentstackGames(
      apiKey,
      managementToken,
      {
        ...queryParams,
        skip: i,
      }
    );
    resultGames.push(...games);
  }

  return resultGames;
}

async function fetchAllEnvironments(apiKey, managementToken, params = {}) {
  const response = await axios(
    "https://eu-api.contentstack.com/v3/environments",
    {
      params,
      headers: {
        api_key: apiKey,
        authorization: managementToken,
        "Content-Type": "application/json",
      },
    }
  );
  return response.data;
}

async function fetchAllContentTypes(apiKey, managementToken, params = {}) {
  const response = await axios(
    "https://eu-api.contentstack.com/v3/content_types",
    {
      params,
      headers: {
        api_key: apiKey,
        authorization: managementToken,
        "Content-Type": "application/json",
      },
    }
  );
  return response.data;
}

async function publishContentstackGames(
  apiKey,
  managementToken,
  contentType,
  entries,
  environments
) {
  const publish = async (entries) => {
    try {
      const response = await axios(
        "https://eu-api.contentstack.com/v3/bulk/publish?skip_workflow_stage_check=true&approvals=true",
        {
          method: "POST",
          data: {
            entries: entries.map((entry) => ({
              uid: entry.uid,
              content_type: contentType,
              version: entry._version,
              locale: "en-gb",
            })),
            locales: ["en-gb"],
            environments: environments,
            publish_with_reference: true,
            // skip_workflow_stage_check: true,
          },
          params: {
            "x-bulk-action": "publish",
          },
          headers: {
            api_key: apiKey,
            authorization: managementToken,
            "Content-Type": "application/json",
          },
        }
      );
      log(
        chalk.white(
          "Status of publishing games:",
          entries.map((game) => ({ code: game.code, uid: game.uid }))
        )
      );
      log("RESPONSE DATA:");
      log(response.data);
      return {
        status: 0,
        response: response.data,
      };
    } catch (e) {
      console.log("********");
      console.log(chalk.red(e.response.data.errors));
      console.log("======");
      // console.log(e.response);
      return {
        status: 1,
        response: e.response.data.errors,
        input: {
          contentType,
          entries,
          environments,
        },
      };
    }
  };


  // TODO remove chunking from here and move this to the service
  let num = 0;

  const flow = Promise.resolve();
  const batches = chunk(entries, 10);
  for (const batch of batches) {
    ++num;
    flow.then(async () => {
      const result = await publish(batch);
      log(
        chalk.white(
          `Status of publishing games: ${batch
            .map((game) => game.code)
            .join(",")}`
        )
      );
      log(chalk.cyan(`number: ${num}`));
      log(chalk.cyan(result));
    });
  }
}

module.exports = {
  fetchAllEnvironments,
  fetchAllContentTypes,
  fetchAllContentstackGames,
  updateGameEntry,
  publishContentstackGames,
};
