require('dotenv').config();
const path = require('path');
const chalk = require('chalk');
const { fetchAllEnvironments } = require('../../utils/contentstack');
const { saveJSONFile, getFormattedTime } = require('../../utils/utils');

const log = console.log;
const dir = path.join(process.cwd(), './output');

let config;

if (
  !process.env.CMS_CONFIG ||
  (process.env.CMS_CONFIG !== 'MC' && process.env.CMS_CONFIG !== 'GAMING')
) {
  log(chalk.red(`Missing or incorrect config variable CMS_CONFIG`));
  process.exit(1);
}

if (process.env.CMS_CONFIG === 'MC') {
  config = require('../../config/mc');
} else if (process.env.CMS_CONFIG === 'GAMING') {
  config = require('../../config/gaming');
}

async function init() {
  log(chalk.blue('Downloading data from CMS. This may take a while...'));

  const apiKey = config.contentstack.apiKey;
  const managementToken = config.contentstack.managementToken;
  const environments = await fetchAllEnvironments(apiKey, managementToken);
  const timestamp = getFormattedTime();

  await saveJSONFile(environments, `all-environments-${process.env.CMS_CONFIG}-${timestamp}`, dir);

  return timestamp;
}

init().then(
  (timestamp) =>
    log(
      chalk.green(`The results have been generated successfully:
  ${dir}/all-environments-${timestamp}.json`)
    ),
  (err) => log(chalk.red(err))
);
