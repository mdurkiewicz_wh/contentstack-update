const fs = require('fs-extra');
const path = require('path');
const { kebabCase } = require('lodash');

async function saveJSONFile(product, id, dir) {
  const stringifiedProduct = JSON.stringify(product, null, 2);

  try {
    await fs.ensureDir(dir);
  } catch (err) {
    console.error(err);
  }

  return await fs.outputFile(
    path.join(dir, `${id}.json`),
    stringifiedProduct,
    'utf8'
  );
}

function getFormattedTime() {
  const date = new Date();
  const y = date.getFullYear();
  const m = date.getMonth() + 1;
  const d = date.getDate();
  const h = date.getHours();
  const min = date.getMinutes();
  const s = date.getSeconds();
  return `${y}-${m}-${d}-${h}-${min}-${s}`;
}

function findHighestTrasactionCode(games) {
  let highestValue = 0;
  if (Array.isArray(games)) {
    games.forEach((game) => {
      if (game.transaction_code && Number(game.transaction_code) > 0) {
        highestValue =
          Number(game.transaction_code) > highestValue
            ? Number(game.transaction_code)
            : highestValue;
      }
    });
  }
  return highestValue;
}

function findDuplicateGameCodes(games, filterKey) {
  let arr = [];
  games.forEach((element) => arr.push(element[filterKey]));
  return arr.filter((item, index) => arr.indexOf(item) != index);
}

function removeVerticalNamePrefix(code) {
  let gameCode = code;
  const verticalNames = [
    'vegas',
    'games',
    'livecasino',
    'casino',
    'bingo',
    'poker',
    'sports',
  ];

  gameCode = gameCode.replace('_', '-');
  // verticalNames.forEach((verticalName) => {
  //   if (gameCode.startsWith(`${verticalName}-`)) {
  //     gameCode = gameCode.replace(`${verticalName}-`, '');
  //   }
  // });

  for (let verticalName of verticalNames) {
    if (gameCode.startsWith(`${verticalName}-`)) {
      gameCode = gameCode.replace(`${verticalName}-`, '');
      break;
    }
  }

  return gameCode;
}

function isValidGameCode(code) {
  // const regex = /[A-Z]|[\s_,]|^\s*$/; // uppercase, whitespace, underscore, comma, empty string
  const regex = /^[a-z\d-]*[a-z|0-9]$/;
  const isNumber = code !== '' && !isNaN(Number(code));
  const isValidFormat = regex.test(code);

  return !isNumber && isValidFormat;
}

function getFormattedPresentationCode(gameCode, gameTitle) {
  let code;

  if (isValidGameCode(gameCode)) {
    // Generate presentation code from game code
    code = removeVerticalNamePrefix(gameCode);
  } else {
    // Generate presentation code from game title
    code = removeVerticalNamePrefix(kebabCase(gameTitle));
  }

  return code;
}

function getGamesWithPresentationCode(allGames, duplicateGameCodes = []) {
  return allGames
    .filter((game) => !duplicateGameCodes.includes(game.code))
    .map((obj) => ({
      code: obj.code,
      title: obj.title,
      uid: obj.uid,
      presentation_code: obj.presentation_code
        ? obj.presentation_code
        : getFormattedPresentationCode(obj.code, obj.title),
      transaction_code: obj.transaction_code || '',
      description: obj.description,
    }));
}

function getAllIncorrectGameCodes(allGames, duplicateGameCodes = []) {
  return allGames
    .filter((game) => !duplicateGameCodes.includes(game.code))
    .filter((game) => !isValidGameCode(game.code))
    .map((obj) => ({
      code: obj.code,
      title: obj.title,
      uid: obj.uid,
      presentation_code: obj.presentation_code
        ? obj.presentation_code
        : getFormattedPresentationCode(obj.code, obj.title),
    }));
}

function findGamesWithNotEmptyMarkets(allGames) {
  const filteredGames = allGames.filter(
    (game) => game.market && game.market.length
  );
  console.log('filteredGames length:', filteredGames.length);
  return filteredGames;
}

function findGamesWithEmptyMarkets(allGames) {
  const filteredGames = allGames.filter(
    (game) => !game.market || (game.market && !game.market.length)
  );
  console.log('filteredGames length:', filteredGames.length);
  return filteredGames;
}

module.exports = {
  saveJSONFile,
  getFormattedTime,
  findGamesWithNotEmptyMarkets,
  findGamesWithEmptyMarkets,
  findHighestTrasactionCode,
  findDuplicateGameCodes,
  getGamesWithPresentationCode,
  getAllIncorrectGameCodes,
};
