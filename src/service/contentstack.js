require("dotenv").config();
const chalk = require("chalk");
const {
  fetchAllEnvironments,
  fetchAllContentTypes,
  fetchAllContentstackGames,
  updateGameEntry,
  publishContentstackGames,
} = require("../utils/contentstack");
const log = console.log;

let config;

if (
  !process.env.CMS_CONFIG ||
  (process.env.CMS_CONFIG !== "MC" && process.env.CMS_CONFIG !== "GAMING")
) {
  log(chalk.red(`Missing or incorrect config variable CMS_CONFIG`));
  process.exit(1);
}

log(chalk.green(`Using config: ${process.env.CMS_CONFIG}`));

if (process.env.CMS_CONFIG === "MC") {
  config = require("../config/mc");
} else if (process.env.CMS_CONFIG === "GAMING") {
  config = require("../config/gaming");
}

const apiKey = config.contentstack.apiKey;
const managementToken = config.contentstack.managementToken;

return (module.exports = {
  fetchAllEnvironments: function (params = {}) {
    log(chalk.cyan("Loading all environment list..."));
    return fetchAllEnvironments(apiKey, managementToken, params);
  },
  fetchAllContentTypes: function (params = {}) {
    return fetchAllContentTypes(apiKey, managementToken, params);
  },
  fetchAllContentstackGames: function (queryParams = {}) {
    log(chalk.cyan("Loading all games..."));
    return fetchAllContentstackGames(apiKey, managementToken, queryParams);
  },
  updateGameEntry: function (entryUID, entryData) {
    return updateGameEntry(apiKey, managementToken, entryUID, entryData);
  },
  publishContentstackGames: async function (entries, environments) {
    //TODO send publish request one by one and check response
    //TODO implement retry capability when error occure; ask for new version to publish (after saving manually in CS new version will be created)
    publishContentstackGames(
      apiKey,
      managementToken,
      "game",
      entries,
      environments
    ).catch((err) => log(chalk.red(err)));
  },
});
