const inquirer = require("inquirer");
const chalk = require("chalk");
const contentstack = require("../../service/contentstack");

const log = console.log;
const int1 = ["nextgennative", "nextgennative-adaptor"];
const int2 = ["nextgennative-adaptor", "nextgennative"];

const exceptions = [
  "bltadaff445f663030d",
  "blt1d47bcb41240bc06",
  "bltadaff445f663030d",
  "blt1d47bcb41240bc06",
  "bltadaff445f663030d",
  "blt1d47bcb41240bc06",
  "bltb2273a19f49c5855",
  "blt19b41cbd3cdce1c2",
];

async function init() {
  const timestamp = Date.now();

  const { environments } = await contentstack.fetchAllEnvironments();
  // log(environments.map((env) => [env.uid, env.name]));

  const { envName } = await inquirer.prompt([
    {
      type: "list",
      message: `Select environment you want to work on:`,
      name: "envName",
      choices: environments.map((e) => e.name),
    },
  ]);

  const env = environments.filter((env) => env.name === envName)[0];
  log(`Environament selected: ${env.name} (${env.uid})\n\n`);

  //TODO make this selection global
  //TODO seperate integration update & publishing task mowing it to separate folders
  const operations = ["Change games integration", "Publish games"];

  const { operation } = await inquirer.prompt([
    {
      type: "list",
      message: "What do you want to do:",
      name: "operation",
      choices: operations,
    },
  ]);

  // UPDATE GAMES
  if (operation === operations[0]) {
    log("\n", chalk.green("Game integration update task"), "\n");

    const { integration } = await inquirer.prompt([
      {
        type: "list",
        message: "Enter integration name to update:",
        name: "integration",
        choices: int1,
      },
    ]);

    const allGames = await contentstack.fetchAllContentstackGames({
      include_count: true,
      include_publish_details: true,
    });

    let filteredGames = allGames.filter(
      (game) => game.launch_configuration.integration === integration
    );

    log(chalk.yellow(`Namber of all games:`), allGames.length);
    log(
      chalk.yellow(`Number of games with integration "${integration}":`),
      filteredGames.length,
      "\n\n"
    );

    const { newIntegration } = await inquirer.prompt([
      {
        type: "list",
        message: "Enter integration name you want to update to: ",
        name: "newIntegration",
        choices: int2,
      },
    ]);

    const { number } = await inquirer.prompt([
      {
        type: "input",
        message: `\n\nHow many games you want to change? (max ${filteredGames.length}): `,
        name: "number",
      },
    ]);

    const part = filteredGames.slice(0, number);

    const { go } = await inquirer.prompt([
      {
        type: "confirm",
        message: `Proceed with change? (${part.length} games)`,
        name: "go",
        default: false,
      },
    ]);

    if (go) {
      log(
        chalk.yellow(
          `Updating integration "${integration}" to "${newIntegration}":`
        )
      );

      part.forEach(async (game, index) => {
        await contentstack
          .updateGameEntry(game.uid, {
            launch_configuration: {
              integration: newIntegration,
            },
          })
          .then((response) => {
            log(
              "--------------",
              index + 1,
              `${game.title}`,
              `(${game.uid})`,
              `Updated\n`,
              response,
              "--------------"
            );
          })
          .catch((err) =>
            log(index + 1, `ERROR (${game.uid})`, chalk.red(err))
          );
      });
    } else {
      log("Skipping game integration update task.\n\n");
    }
  }

  // PUBLISH GAMES
  if (operation === operations[1]) {
    log("\n", chalk.green("Game publish task"), "\n");

    const { integration } = await inquirer.prompt([
      {
        type: "list",
        message: "Enter integration name to search:",
        name: "integration",
        choices: int1,
      },
    ]);

    const allGames = await contentstack.fetchAllContentstackGames({
      include_count: true,
      include_publish_details: true,
    });

    log(chalk.yellow(`Namber of all games:`), allGames.length);

    const filteredGames = allGames.filter(
      (game) => game.launch_configuration.integration === integration
    );

    log(
      chalk.yellow(`Number of games with integration "${integration}":`),
      filteredGames.length
    );

    const gamesToPublish = filteredGames.filter(
      (game) =>
        !!game.publish_details.find(
          (el) =>
            el.environment === env.uid &&
            el.version < game._version &&
            !exceptions.includes(game.uid)
        )
    );

    log(
      chalk.yellow(`Number of games eligable for publish:`),
      gamesToPublish.length
    );

    log("\n\n");
    const { number } = await inquirer.prompt([
      {
        type: "input",
        message: `How many games you want to publish? (max ${gamesToPublish.length}): `,
        name: "number",
        default: 1,
      },
    ]);

    const part = gamesToPublish.slice(
      0,
      number < gamesToPublish.length ? number : gamesToPublish.length
    );

    const { go } = await inquirer.prompt([
      {
        type: "confirm",
        message: `Proceed with publish? (${part.length} games)`,
        name: "go",
        default: false,
      },
    ]);

    if (go) {
      log(
        chalk.cyan(
          `Publishing ${part.length} records to ${env.name} (${env.uid})...`
        )
      );

      const { details } = await inquirer.prompt([
        {
          type: "confirm",
          message: `Should it log publish input summary?`,
          name: "details",
          default: true,
        },
      ]);

      if (details) {
        part.map((game) => {
          const el = game.publish_details.find(
            (el) => el.environment === env.uid
          );
          log(
            `${game.uid} - latest:${game._version}, published:${
              el ? el.version : "n/a"
            } - ${game.title}`
          );
        });
      }

      await contentstack.publishContentstackGames(part, [env.uid]);
    } else {
      log("Skipping games publish task.\n\n");
    }
  }

  return (Date.now() - timestamp) / 1000;
}

init().then(
  (t) => log(chalk.green(`\n\nRecords updated (${t}s).`)),
  (err) => log(chalk.red(err))
);
