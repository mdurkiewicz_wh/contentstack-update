## [Contentstack] Migration of presentation codes and transaction codes

# https://jira.willhillatlas.com/browse/GPORT-11937

# https://conf.willhillatlas.com/pages/viewpage.action?spaceKey=GAMINGTEAM&title=Game+model+-+reduction+of+game+codes


## Environments Contentstack uid / Gaming

```
{
  environments: [
    {
      name: 'prod',
      uid: 'blt0a33f6296b4f887e',
    },
    {
      name: 'nonprod',
      uid: 'blt111a8051697d8a2b',
    },
    {
      name: 'dev',
      uid: 'blt06375450ade39e7a',
    }
  ]
}
```

## Environments Contentstack uid / MC

```
{
  environments: [
    {
      "name": "prod",
      "uid": "bltec17ad5ea7158599",
    },
    {
      "name": "nonprod",
      "uid": "blt43f1351e4fd2caf8",
    },
    {
      "name": "dev",
      "uid": "bltef3fffc716acee33",
    }
  ]
}
```