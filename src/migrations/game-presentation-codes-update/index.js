require('dotenv').config();
const path = require('path');
const chalk = require('chalk');
const {
  fetchAllContentstackGames,
  updateGameEntry,
} = require('../../utils/contentstack');
const {
  saveJSONFile,
  getFormattedTime,
  findHighestTrasactionCode,
  findDuplicateGameCodes,
  getGamesWithPresentationCode,
  getAllIncorrectGameCodes,
} = require('../../utils/utils');
const log = console.log;

const dir = path.join(process.cwd(), './output');

let config;

if (
  !process.env.CMS_CONFIG ||
  (process.env.CMS_CONFIG !== 'MC' && process.env.CMS_CONFIG !== 'GAMING')
) {
  log(chalk.red(`Missing or incorrect config variable CMS_CONFIG`));
  process.exit(1);
}

if (process.env.CMS_CONFIG === 'MC') {
  config = require('../../config/mc');
} else if (process.env.CMS_CONFIG === 'GAMING') {
  config = require('../../config/gaming');
}

async function init() {
  log(chalk.blue('Downloading data from CMS. This may take a while...'));

  const apiKey = config.contentstack.apiKey;
  const managementToken = config.contentstack.managementToken;
  const timestamp = getFormattedTime();
  const allGames = await fetchAllContentstackGames(apiKey, managementToken, {
    include_count: true,
    include_publish_details: true,
  });
  const highestTransacionCodeValue = findHighestTrasactionCode(allGames);

  log(chalk.cyan(`Number of games fetched from the CMS: ${allGames.length}`));

  // ## STAGE 1 - filter out duplicate legacy game codes
  const duplicateGameCodes = findDuplicateGameCodes(allGames, 'code');

  await saveJSONFile(allGames, `allGames-game-codes-${timestamp}`, dir);

  log(
    chalk.cyan(`Number of duplicate game codes: ${duplicateGameCodes.length}`)
  );

  await saveJSONFile(
    duplicateGameCodes,
    `duplicated-game-codes-${timestamp}`,
    dir
  );

  // ## STAGE 2 - create array of incorrect game code for verification
  const incorrectGameCodes = getAllIncorrectGameCodes(
    allGames,
    duplicateGameCodes
  );

  log(
    chalk.cyan(`Number of incorrect game codes: ${incorrectGameCodes.length}`)
  );

  await saveJSONFile(
    incorrectGameCodes,
    `incorrect-game-codes-${timestamp}`,
    dir
  );

  // const allGamesWithNoPresentationCode = allGames.filter(
  //   (game) => game.presentation_code === '' || !game.presentation_code
  // );
  // log(
  //   chalk.cyan(
  //     `Number of games with no presentation codes: ${allGamesWithNoPresentationCode.length}`
  //   )
  // );

  // ## STAGE 3 - create array of games with presentation code
  const gamesWithPresentationCode = getGamesWithPresentationCode(
    allGames,
    duplicateGameCodes
  );

  // ## STAGE 4 - filter out duplicate presentation codes
  const duplicatePresentationCodes = findDuplicateGameCodes(
    gamesWithPresentationCode,
    'presentation_code'
  );

  log(
    chalk.cyan(
      `Number of duplicate presentation codes: ${duplicatePresentationCodes.length}`
    )
  );

  await saveJSONFile(
    duplicatePresentationCodes,
    `duplicated-presentation-codes-${timestamp}`,
    dir
  );

  // Test

  const test = gamesWithPresentationCode
    .filter((game) =>
      duplicatePresentationCodes.includes(game.presentation_code)
    )
    .map((obj) => {
      return {
        code: obj.code,
        title: obj.title,
        uid: obj.uid,
        presentation_code: obj.presentation_code,
      };
    });
  await saveJSONFile(test, `test-incorrect-games-${timestamp}`, dir);

  // ## STAGE 5 - create array of games with unique presentation codes and transaction code
  let transactionCode = highestTransacionCodeValue;

  const filteredGames = gamesWithPresentationCode
    .filter(
      (game) => !duplicatePresentationCodes.includes(game.presentation_code)
    )
    .map((obj) => {
      if (!obj.transaction_code) ++transactionCode;
      return {
        code: obj.code,
        title: obj.title,
        uid: obj.uid,
        presentation_code: obj.presentation_code,
        transaction_code: obj.transaction_code
          ? obj.transaction_code
          : transactionCode,
        description: obj.description,
      };
    });

  await saveJSONFile(filteredGames, `all-updated-games-${timestamp}`, dir);

  log(
    chalk.cyan(
      `Highest occurring transacion code value in the CMS: ${highestTransacionCodeValue}`
    )
  );

  log(chalk.cyan(`Number of games to update: ${filteredGames.length}`));

  // ## STAGE 6 - update games data in CMS
  // const j = filteredGames.length;
  // for (let i = 0; i < j; i++) {
  //   await updateGameEntry(apiKey, managementToken, filteredGames[i].uid, {
  //     presentation_code: filteredGames[i].presentation_code,
  //     transaction_code: filteredGames[i].transaction_code,
  //   });
  //   log(chalk.reset(`--- udpated game nr: ${i + 1}`));
  // }

  return timestamp;
}

init().then(
  (timestamp) =>
    log(
      chalk.green(
        `The result files were successfully generated:
  * ${dir}/all-updated-games-${timestamp}.json
  * ${dir}/duplicated-game-codes-${timestamp}.json
  * ${dir}/duplicated-presentation-codes-${timestamp}.json
  `
      )
    ),
  (err) => log(chalk.red(err))
);
