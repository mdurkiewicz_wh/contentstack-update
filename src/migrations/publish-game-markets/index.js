require('dotenv').config();
const path = require('path');
const chalk = require('chalk');
const {
  fetchAllContentstackGames,
  updateGameEntry,
} = require('../../utils/contentstack');
const {
  saveJSONFile,
  getFormattedTime,
  findGamesWithNotEmptyMarkets,
  findGamesWithEmptyMarkets,
} = require('../../utils/utils');
const log = console.log;

const dir = path.join(process.cwd(), './output');

let config;

if (
  !process.env.CMS_CONFIG ||
  (process.env.CMS_CONFIG !== 'MC' && process.env.CMS_CONFIG !== 'GAMING')
) {
  log(chalk.red(`Missing or incorrect config variable CMS_CONFIG`));
  process.exit(1);
}

if (process.env.CMS_CONFIG === 'MC') {
  config = require('../../config/mc');
} else if (process.env.CMS_CONFIG === 'GAMING') {
  config = require('../../config/gaming');
}

async function init() {
  log(chalk.blue('Downloading data from CMS. This may take a while...'));

  const apiKey = config.contentstack.apiKey;
  const managementToken = config.contentstack.managementToken;
  const timestamp = getFormattedTime();
  const allGames = await fetchAllContentstackGames(apiKey, managementToken, {
    include_count: true,
    include_publish_details: true,
    locale: 'en-gb',
  });

  log(chalk.cyan(`Number of games fetched from the CMS: ${allGames.length}`));

  // await saveJSONFile(
  //   allGames,
  //   `all-games-${process.env.CMS_CONFIG}-${timestamp}`,
  //   dir
  // );

  // ## STAGE 1
  // const filteredGames = findGamesWithNotEmptyMarkets(allGames);
  // await saveJSONFile(
  //   filteredGames,
  //   `games-with-markets-${process.env.CMS_CONFIG}-${timestamp}`,
  //   dir
  // );
  // log(
  //   chalk.cyan(`Number of games with Markets: ${filteredGames.length}`)
  // );

  const filteredGames = findGamesWithEmptyMarkets(allGames);
  await saveJSONFile(
    filteredGames,
    `games-with-empty-markets-${process.env.CMS_CONFIG}-${timestamp}`,
    dir
  );
  log(
    chalk.cyan(`Number of games with empty Markets: ${filteredGames.length}`)
  );

  // await saveJSONFile(allGames, `allGames-game-codes-${timestamp}`, dir);

  // ## STAGE 2 - update games data in CMS

  // await updateGameEntry(apiKey, managementToken, 'bltd9f81b1839decbe7', {
  //   market: {
  //     PUSH: {
  //       data: [
  //         {
  //           uid: 'bltb0be345d6e7f1413',
  //           _content_type_uid: 'market',
  //         },
  //         {
  //           uid: 'blte2307dd5a1f4cf06',
  //           _content_type_uid: 'market',
  //         },
  //       ],
  //     },
  //   },
  // });

  const j = filteredGames.length;
  for (let i = 0; i < j; i++) {
    // MC
    // await updateGameEntry(apiKey, managementToken, filteredGames[i].uid, {
    //   market: {
    //     PUSH: {
    //       data: [
    //         {
    //           uid: 'blt0c35cb51ceb0a039',
    //           _content_type_uid: 'row_v1_market',
    //         },
    //         {
    //           uid: 'blt7cdc6b0c4e1ed2a5',
    //           _content_type_uid: 'row_v1_market',
    //         },
    //       ],
    //     },
    //   },
    // });


    // GAMING
    await updateGameEntry(apiKey, managementToken, filteredGames[i].uid, {
      market: {
        PUSH: {
          data: [
            {
              uid: 'bltb0be345d6e7f1413',
              _content_type_uid: 'market',
            },
            {
              uid: 'blte2307dd5a1f4cf06',
              _content_type_uid: 'market',
            },
          ],
        },
      },
    });

    log(chalk.reset(`--- udpated game nr: ${i + 1} / uid: ${filteredGames[i].uid}`));
  }

  return timestamp;
}

init().then(
  (timestamp) =>
    log(
      chalk.green(
        `Job has been finished successfully: ${timestamp}
  `
      )
    ),
  (err) => log(chalk.red(err))
);
