require('dotenv').config();
const path = require('path');
const chalk = require('chalk');
const {
  fetchAllContentstackGames,
  publishContentstackGames,
} = require('../../utils/contentstack');
const { saveJSONFile, getFormattedTime } = require('../../utils/utils');
const log = console.log;

const dir = path.join(process.cwd(), './output');

let config;

if (
  !process.env.CMS_CONFIG ||
  (process.env.CMS_CONFIG !== 'MC' && process.env.CMS_CONFIG !== 'GAMING')
) {
  log(chalk.red(`Missing or incorrect config variable CMS_CONFIG`));
  process.exit(1);
}

if (process.env.CMS_CONFIG === 'MC') {
  config = require('../../config/mc');
} else if (process.env.CMS_CONFIG === 'GAMING') {
  config = require('../../config/gaming');
}

async function init() {
  log(chalk.blue('Downloading data from CMS. This may take a while...'));

  const apiKey = config.contentstack.apiKey;
  const managementToken = config.contentstack.managementToken;
  const cmsEnvUID = config.publish.environment;
  const timestamp = getFormattedTime();
  const allGames = await fetchAllContentstackGames(apiKey, managementToken, {
    include_count: true,
    include_publish_details: true,
  });
  // Find games published for provided environment.
  const filteredGames = allGames
    .filter(
      (game) =>
        !!game.publish_details.find((el) => el.environment === cmsEnvUID)
    )
    .map((el) => ({
      _version: el._version,
      code: el.code,
      uid: el.uid,
    }));

  log(chalk.cyan(`apiKey: ${apiKey}`));
  log(chalk.cyan(`managementToken: ${managementToken}`));

  log(chalk.cyan(`Number of games fetched from the CMS: ${allGames.length}`));
  log(
    chalk.cyan(`Number of games filtered to publish: ${filteredGames.length}`)
  );

  await saveJSONFile(
    filteredGames,
    `game-codes-to-publish-${process.env.CMS_CONFIG}-prod-${cmsEnvUID}-${timestamp}`,
    dir
  );

  await publishContentstackGames(
    apiKey,
    managementToken,
    'game',
    filteredGames,
    [cmsEnvUID]
  ).then(() => console.log('Done'));

  return { cmsEnvUID, timestamp };
}

init().then(
  (response) =>
    log(
      chalk.green(
        `List of games submitted for publication :
  * ${dir}/game-codes-to-publish-${response.cmsEnvUID}-${response.timestamp}.json
  `
      )
    ),
  (err) => log(chalk.red(err))
);
